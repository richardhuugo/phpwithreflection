<!DOCTYPE HTML>
<html lang="en-US">
  <head>
    <?php $this->load->view('header'); ?>
  </head>
<body>		
	<div class="container">
	  <div class="col-md-6 jumbotron">
				<?php	
		  			echo form_open("pagina/contato");
	  			?>
				<div>

					<table>
						<tbody>
							<tr>
								<td>
									<?php 
									echo form_label("Seu nome:","nome") . "<br />";
									echo form_input("nome",set_value("nome"));
									?>
								</td>
							</tr>
							<tr>
								<td>
									<?php
									echo form_label("E-Mail:","email") . "<br />";
									echo form_input("email",set_value("email"));
									?>
								</td>
							</tr>
							<tr>
								<td>
									<?php
									echo form_label("Assunto:","assunto") . "<br />";
									echo form_input("assunto",set_value("assunto"));
									?>
								</td>
							</tr>
							<tr>
								<td>
									<?php
									echo form_label("Mensagem:","mensagem") . "<br />";
									echo form_textarea("mensagem",set_value("mensagem"));
									?>
								</td>
							</tr>
							<tr>
								<td>
									<br />
									<?php
									echo form_submit("enviar:","Enviar mensagem", array("class" => "btn btn-primary btn-xs"));
									?>									
								</td>
							</tr>							
						</tbody>
					</table>								

					<?php echo form_close(); ?>
				</div>
		</div>
				<div class="col-md-6">
					<?php	
	 		  			if ($formerror){?>
	 		  				<br />
							<div class="alert alert-dismissible alert-danger" role="alert">
				  				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				  				<span aria-hidden="true">&times;</span></button>
				 			 	<strong>Error</strong> 
				  			 	<div>
				  			 		<?php echo $formerror; ?>	
				  			 	</div>
				  			</div>
					  				
			  		<?php }?>
		  		</div>		
	</div>

	<footer>
		<?php $this->load->view('footer'); ?>
	</footer>
</body>
</html>