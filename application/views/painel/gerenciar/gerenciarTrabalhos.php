<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php $this->load->view('header'); ?>
	
</head>
<body>
	
<div class="container">	  
	  <div class="row">
			<div class="col-md-12">
				<div class="col-md-4">
					<form action="" method="get">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h3 class="panel-title">Novo Trabalho</h3>
							</div>
							<div class="panel-body">
								<center>
    								<form ACTION="CollaboratorList.php" METHOD="GET">
    									<label>Nome</label> 
										<input type="text" name="CollaboratorName"> 
										<input type="submit" class="btn btn-primary btn-xs" name="Action" value="Salvar">
    								</form>
								</center>
							</div>
						</div>
					</form>
				</div>
	      
				
					
	  <div class="jumbotron col-md-12">
					<h3>Colaboradores</h3>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th colspan="2">Titulo</th>								
							</tr>
						</thead>
						<tbody>
                            <tr>
                                <td>1</td>
                                <td>Teste 1</td>
                                <td>Editar e Deletar</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Teste 2</td>
                                <td>Editar e Deletar</td>
                            </tr>
                        </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- /container -->


	<br />
	<?php $this->load->view('footer'); ?>
</body>
</html>