<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php $this->load->view('header'); ?>
	
</head>
<body>
	
<div class="container">	  
	  <div class="row">
			<div class="col-md-12">
	
<?php	
	$dadosModal['titulo'] = 'Adicionar Trabalho';
	$dadosModal['redirect'] = 'gerenciarTrabalhos';
	 $this->load->view('painel/gerenciar/modalPage',$dadosModal);
?>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Adicionar novo trabalho</button>
<br /><br />


<?php echo get_msg(); ?>

	  <div class="jumbotron col-md-12">
					<p><?php echo $Titulo; ?></p>

					<table class="table table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th colspan="2">Titulo</th>								
							</tr>
						</thead>
						<tbody>
                            <tr>
                                <td>1</td>
                                <td>Teste 1eeeeeeeeeeeeeeeeeeeeeee</td>
                                <td>
									<a href="javascript:{}" onclick="document.getElementById('').submit(); return false;" >
										<span class="glyphicon glyphicon-edit btn btn-xs" aria-hidden="true"></span>
									</a>
									<a href="javascript:{}" onclick="document.getElementById('').submit(); return false;" >
										<span class="glyphicon glyphicon-trash btn btn-xs" aria-hidden="true"></span>
									</a>
								</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Teste 2aaaaaaaaaaaa</td>
                                <td>
									<a href="javascript:{}" onclick="document.getElementById('').submit(); return false;" >
										<span class="glyphicon glyphicon-edit btn btn-xs" aria-hidden="true"></span>
									</a>
									<a href="javascript:{}" onclick="document.getElementById('').submit(); return false;" >
										<span class="glyphicon glyphicon-trash btn btn-xs" aria-hidden="true"></span>
									</a>
								</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Teste 3aaaaaaaaaaaaaaaaaaaaa</td>
                                <td>
									<a href="javascript:{}" onclick="document.getElementById('').submit(); return false;" >
										<span class="glyphicon glyphicon-edit btn btn-xs" aria-hidden="true"></span>
									</a>
									<a href="javascript:{}" onclick="document.getElementById('').submit(); return false;" >
										<span class="glyphicon glyphicon-trash btn btn-xs" aria-hidden="true"></span>
									</a>
								</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Teste 4sssssssssssssssssssss</td>
                                <td>
									<a href="javascript:{}" onclick="document.getElementById('').submit(); return false;" >
										<span class="glyphicon glyphicon-edit btn btn-xs" aria-hidden="true"></span>
									</a>
									<a href="javascript:{}" onclick="document.getElementById('').submit(); return false;" >
										<span class="glyphicon glyphicon-trash btn btn-xs" aria-hidden="true"></span>
									</a>
								</td>
                            </tr>														
                        </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- /container -->

	<br />
	<?php $this->load->view('footer'); ?>
</body>
</html>



