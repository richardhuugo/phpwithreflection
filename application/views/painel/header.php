<!DOCTYPE HTML>
<html lang="en-US">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Custom styles for this template -->
    <link href="navbar-static-top.css" rel="stylesheet">
	<link href="<?php echo base_url("assets/bootstrap-3.3.7-dist/css/bootstrap.min.css"); ?>" rel="stylesheet">	
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"> </script>
    <script src="<?php echo base_url("assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"); ?>"> </script>
	
	
	<title><?php echo $Titulo; ?> - Painel</title>
	
  </head>
  
<body>
    <!-- Static navbar -->
    <nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <span class="navbar-brand">Estudo PHP (CodeIgniter)</span>
        </div>
        <div id="navbar" class="navbar-collapse collapse">

          <?php 
          if (isset($controller)){
          ?>
            <ul class="nav navbar-nav navbar-right">
              <?php 
              if ((isset($login)) && ($login != null)){?>
                <li><a href="#"><sapn class="glyphicon glyphicon-user"> </span>Login</a></li>  
              <?php }else{?>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Flávio Wagner <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#"><sapn class="glyphicon glyphicon-user"> </span> Perfil</a></li>
                  <li><a href="#"><sapn class="glyphicon glyphicon glyphicon-cog"> </span> Gerenciar</a></li>
                  <li><a href="#"><sapn class="glyphicon glyphicon glyphicon-pencil"> </span> Pendências</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="#"><sapn class="glyphicon  glyphicon-remove-circle"> </span> Sair</a></li>
                </ul>
              </li>
              <?php }?>
            </ul>
          <?php }?>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
</body>
</html>    