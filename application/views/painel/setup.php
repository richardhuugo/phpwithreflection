<!DOCTYPE HTML>
<html lang="en-US">
  <head>
    <?php $dados['Titulo'] = "Cadastrar Acesso"; $this->load->view('header',$dados); ?>
  </head>
<body>	
    <div class="container">
        <div class='col-md-4'>
            <div class = 'panel panel-primary'>
                <div class="panel-heading">
                    <?php echo $h2; ?>
                </div>
                
                <?php echo form_open(); ?> 

                <ul class="list-group">
                    <li class="list-group-item">

                        <?php if ($msg = get_msg()){ ?>
                            <div class="panel-body">
                                <div class="alert alert-danger">
                                    <?php echo $msg; ?>
                                </div>
                            </div>
                        <?php }?>  

                        <?php 
                            echo form_label('Nome para login', 'login') .'<br />';
                            echo form_input('login', set_value('login'),array('autofocus' => 'autofocus' ));
                        ?> 
                    </li>
                    <li class="list-group-item">
                        <?php 
                            echo form_label('E-mail do administrador', 'email') .'<br />';
                            echo form_input('email', set_value('email'));
                        ?> 
                    </li>
                    <li class="list-group-item">
                        <?php 
                            echo form_label('senha', 'senha') .'<br />';
                            echo form_password('senha', set_value('senha') );
                        ?> 
                    </li>
                    <li class="list-group-item">
                        <?php 
                            echo form_label('Repita a senha', 'senha2') .'<br />';
                            echo form_password('senha2', set_value('senha2'));
                        ?> 
                    </li> 
                </ul>
                <div class="container">
                    <p>
                        <?php 
                            echo form_submit('enviar','Salvar Dados',array('class' => 'btn btn-primary'));
                            echo form_close(); 
                        ?> 
                    </p>
                <div>
            </div>
        </div>
    </div>            
	<br />
	<footer>
		<?php $this->load->view('footer'); ?>
	</footer>
</body>
</html>