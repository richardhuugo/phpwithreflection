<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagina extends CI_Controller {
    function __construct(){
        parent::__construct();
	}
	
	public function index()
	{
		$dados["Titulo"] = "Pagina Principal";
		$dados["controller"] = "home";
		$this->load->view('home',$dados);
	}
	public function trabalhos()
	{
		$dados["Titulo"] = "Trabalhos";
		$dados["controller"] = "trabalhos";
		$this->load->view('trabalhos',$dados);
	}
	public function noticias()
	{
		$dados["Titulo"] = "Noticias";
		$dados["controller"] = "noticias";
		$this->load->view('noticias',$dados);
	}
	public function sobre()
	{
		$dados["Titulo"] = "Sobre";
		$dados["controller"] = "sobre";
		$this->load->view('sobre',$dados);
	}
	public function contato()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('nome','Nome','trim|required');
		$this->form_validation->set_rules('email','E-Mail','trim|required|valid_email');
		$this->form_validation->set_rules('assunto','Assunto','trim|required');
		$this->form_validation->set_rules('mensagem','Mensagem','trim|required');

		if (! $this->form_validation->run()) {
			$dados["formerror"] = validation_errors();
		} else {			
			try {
				/*
				$dados["formerror"] = null;

				$dados_form = $this->input->post();

				//Enviar email			
				$this->load->library('email');
				$config['protocol']    = 'smtp';
		        $config['smtp_host']    = 'ssl://smtp.gmail.com';
		        $config['smtp_port']    = '465';
		        $config['smtp_timeout'] = '7';
		        $config['smtp_user']    = 'flaviowagner@gmail.com';
		        $config['smtp_pass']    = '01641915188';
		        $config['charset']    = 'utf-8';
		        $config['mailtype'] = 'text'; // or html        
		        $config['validation'] = TRUE; // bool whether to validate email or not      
		        $this->email->initialize($config);
				*/
		        
	        } catch (Exception $e) {
			}

		}
		
		
		$dados["Titulo"] = "Contato";
		$dados["controller"] = "contato";
		$this->load->view('contato',$dados);
	}
	
}
