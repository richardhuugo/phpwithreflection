<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setup extends CI_Controller {
    public function __construct(){
        parent::__construct();
    
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('Option_model','option');
	}

	public function index(){	
		if ( $this->option->get_option('setup_executado') == 1) {
			redirect('setup/alterar','refresh');
		}
		else{
			redirect('setup/instalar','refresh');
		}				
	}

	public function instalar(){
		if($this->option->get_option('setup_executado') == 1){
			redirect('setup/alterar','refresh');
		}

		//validar mformulario
		$this->form_validation->set_rules('login','Login','trim|required');
		$this->form_validation->set_rules('email','E-Mail','trim|required|valid_email');
		$this->form_validation->set_rules('senha','Senha','trim|required|min_length[6]');
		$this->form_validation->set_rules('senha2','Repita a senha','trim|required|min_length[6]|matches[senha]');

		if (!$this->form_validation->run()) {
			if (validation_errors()) {
				set_msg(validation_errors());
			} 
			
		} else {
			$dados_form = $this->input->post();
			$this->option->update_option('user_login',$dados_form['login']);
			$this->option->update_option('user_email',$dados_form['email']);
			$this->option->update_option('user_pass',password_hash( $dados_form['senha'], PASSWORD_DEFAULT));

			$inserido = $this->option->update_option('setup_executado',1);
			if($inserido){
				set_msg('<p>Efetue o login!</p>');
				redirect('setup/login','refresh');
			}			
		}

		$dados['Titulo'] = 'ok';
		$dados['h2'] = 'Cadastrar novo usuário';
		$this->load->view('painel/setup',$dados);
	}

	public function login(){
		if($this->option->get_option('setup_executado') != 1){
			redirect('setup/instalar','refresh');
		}

		//validar mformulario
		$this->form_validation->set_rules('login','Login','trim|required');
		$this->form_validation->set_rules('senha','Senha','trim|required|min_length[6]');
		
		$dados['erro'] = 0;

		if (!$this->form_validation->run()) {
			if (validation_errors()) {
				set_msg(validation_errors());
				$dados['erro'] = 1;
			} 
			
		} else {
			$dados_form = $this->input->post();
			if ($this->option->get_option('user_login') == $dados_form['login']) {
				if(password_verify($dados_form['senha'], $this->option->get_option('user_pass'))){
					$this->session->userdata('logged',TRUE);
					$this->session->userdata('user_login',$dados_form['login']);
					$this->session->userdata('user_email',$this->option->get_option('user_email'));

					set_msg('<p>Senha ok!</p>');

					var_dump($_SESSION);
				}else{
					set_msg('<p>Senha incorreta!</p>');
					$dados['erro'] = 1;
				}
			}else{
				set_msg('<p>Usuário não existe!</p>');
				$dados['erro'] = 1;
			}
		}
		
		$dados['Titulo'] = 'CodeIgniter';
		$dados['h2'] = 'Acessar o painel';
		$this->load->view('painel/login',$dados);
	}	

	public function alterar(){
		//verificar login
		verifica_login();

		$dados['Titulo'] = 'CodeIgniter - Configuração do sistema';
		$dados['h2'] = 'Alterar configuração';
		$this->load->view('painel/config',$dados);
	}


	public function logout(){
		$this->session->unset_user_data('logged');
		$this->session->unset_user_data('user_login');
		$this->session->unset_user_data('user_email');
		set_msg('<p>Efetue o login</p>');
		redirect('setup/login','refresh');
	}	

	public function gerenciarTrabalhos(){
        //verifica o login
		verifica_login();

		
		redirect('setup/login','refresh');
	}
}
