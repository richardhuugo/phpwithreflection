<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagina extends CI_Controller {
    function __construct(){
        parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function salvar($redirect = 'Pagina')
	{
        redirect($redirect,'refresh');
		
		$this->form_validation->set_rules('titulo','titulo','trim|required');
		$this->form_validation->set_rules('descricao','descricao','trim|required');

		if (! $this->form_validation->run()) {
			set_msg(validation_errors());
		} else {			
			set_msg('OK....');
		}

		$dadosModal['titulo'] = 'Adicionar Trabalho';
		$dadosModal['redirect'] = 'gerenciarTrabalhos';
		$this->load->view('painel/gerenciar/modalPage',$dadosModal);
	}    
}