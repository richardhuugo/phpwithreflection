<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GerenciarTrabalhos extends CI_Controller {
    function __construct(){
        parent::__construct();
			$this->load->helper('form');
			$this->load->library('form_validation');
	}

	
	public function index()
	{
		$this->form_validation->set_rules('titulo','Tirulo','trim|required');
		$this->form_validation->set_rules('descricao','Descrição','trim|required');
		
		if (! $this->form_validation->run()) {
			set_msg(validation_errors());echo "11111111111111111111";
		} else {			
			set_msg('OK....');echo "2222222222222222";
			
		}

		$dados["Titulo"] = "Gerenciar Trabalhos";
		$dados["controller"] = "trabalhos";
		$this->load->view('painel/gerenciar/listarTrabalhos',$dados);
	}    

    public function listarTrabalhos()
	{


		$dados["Titulo"] = "Gerenciar Trabalhos";
		$dados["controller"] = "trabalhos";
		$this->load->view('painel/gerenciar/listarTrabalhos',$dados);
	}
}